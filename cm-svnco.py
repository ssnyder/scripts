#!/usr/bin/env python

# @file:    svnco.py
# @purpose: Checkout a given package. Find container names and release tag
#           if not explicitly given.
# @author:  Frank Winklmeier, Hans von der Schmitt, Johannes Elmsheuser
#
################

# setup e.g. with asetup gcc48,here,none

__version__ = "$Revision: 0.92 $"
__author__  = "Frank Winklmeier, Hans von der Schmitt, Johannes Elmsheuser"

import sys
import os
import time
import getopt
import string
import subprocess
import pickle
import re

# some globals
try:
   import multiprocessing as mp
except ImportError:
   mp = None
mp = None  # for now

trunkortag="_trunk_tags"
repofile="repofile"
maxage=3600  # repo file too old if > 1hr
alsogaudi=["PartPropSvc", "RootCnv", "RootHistCnv"]



class preper:

# prepare some global data (some could come from the environmt but set constant for now)
  def __init__(self):

# list of projects
    self.projects=["AtlasCore", "AtlasConditions", "AtlasEvent", 
                   "AtlasReconstruction", "AtlasTrigger", "AtlasAnalysis", 
                   "AtlasSimulation", "AtlasOffline", "AtlasProduction", 
                   "DetCommon", "AtlasHLT", "GAUDI", "AtlasDerivation"
                   ]
# path to projects
    self.distatlas="svn+ssh://svn.cern.ch/reps/atlasoff/"       # should be same as $SVNROOT (except trailing /)
    self.distgaudi="svn+ssh://svn.cern.ch/reps/gaudi/Gaudi/"
# preset package versions cache (set later by do_cache to hold entire release)
    self.cachelist=[]
# options - are modified in __main__ according to cmd-line options
    self.o_detail=False
    self.o_detail2=False
    self.o_head = True
    self.o_version = None
    self.o_pkgfile = None
    self.o_docheckout = True
    self.o_showrecent = False
    self.o_pathlist = False
    self.o_repofl = False
    self.o_nocache = True
    self.o_tagcmake = False
# get svn root
    self.env = dict(os.environ)
    self.svnroot = self.env.get("SVNROOT", None)
    if self.svnroot is None:
      raise RuntimeError, "SVNROOT is not set - exit."
    self.svnroot += "/"
    if self.svnroot != self.distatlas:
      info("Assumption about svn root seems out of date. Preset: %s, $SVNROOT: %s" %(self.distatlas, self.svnroot))
      info("Using $SVNROOT for athena packages. Gaudi packages remain at their preset: %s" %self.distgaudi)
      self.distatlas=self.svnroot


def usage():
   print """\
Usage: svnco.py [OPTION]... PACKAGE...

Checkout PACKAGE from the release. Possible formats:
  Package                  find container and checkout head tag
  Package-X-Y-Z            find container and checkout specified tag
  Container/Package        checkout head tag (still a / if no container)
  Container/Package-X-Y-Z  checkout specified tag ( " )

Setup e.g. with: asetup gcc48,here,none

where OPTION is:
  -r vers specify package version (trunk is default).
          this works only for single package checkout. 
          in case of multiple packages, specify the version instead of the 
          bare package name
  -c      do checkout (default, so this option need not be given)
  -s      show package information, no checkout
  -f file file contains package list (one per line)
          these packages are added to the ones from the command line
  -d      debug mode - dry running (no checkout) with diagnostic output
  -x      debug mode - dry running (no checkout) with very verbose output
  -n      work without repository: provide comma-separated 
          pairs of path,container/package (also in file with -f).
          usable only for checkout
  -u      read contents of repository from file rather that constructing 
          it fresh from svn, but make fresh file if it is older than 1hr.
          it is a pickle file named repofile, located in pwd
  -t      Checkout the tag from the current release
  -v      show version of this script and exit
  -h      print this and exit
"""
   return


def info(str):
# informative output
  print "-----", str


def detail(c, str):
# optional detailed output
  if c.o_detail:
    print "--d--", str
  return c.o_detail


def do_cache(c):

# generate list of athena packages from the use stmts in <project>Release/cmt/requirements
# and for gaudi packages, from directory list
#
# ie this is a cmt remnant
#
# format of cachelist:  [<project>, <container>, <package>, <package_trunk_version>, <path>]
#
# format of athena path within repository: atlasoff/<package>/trunk
#                                          atlasoff/<package>/tags/<version>
# and for gaudi:                           gaudi/Gaudi/trunk/<package>
#                                          gaudi/Gaudi/tags/<package>/<version>

  detail(c, "do_cache called")

  if c.o_repofl:      # read cachelist from file
    try:
      f = open(repofile)
      rage = time.time()-os.path.getmtime(repofile)
      if rage < maxage:
        c.cachelist=pickle.load(f)
        f.close()
        detail(c, "Repository contents was read from file %s" %repofile)
        detail(c, "packages found: %d" %(len(c.cachelist)))
        if c.o_detail2:
          detail(c, "sorted list of packages from release trunk [project, container, package, version, path]:")
          for l in c.cachelist:
            detail(c, l)
        return
      else:
        info("File %s is older than %d sec. Regenerate it" %(repofile,maxage))
    except IOError:   # else continue into generating new one
      info("Cannot open file '%s'. Regenerate it" % repofile)

  # Look into CMAKE_PREFIX_PATH environment
  cpath = os.getenv( "CMAKE_PREFIX_PATH" )
  if not cpath:
     info( "ERROR: CMAKE_PREFIX_PATH empty - no CMake release seems to be set up" )
     useCMake = False
  else:
     useCMake = True

  pacentered=[]
  for pp in c.projects:

    # Use package.txt for CMake release and skip SVN read 
    if useCMake:
       c.cachelist = get_version_all(c)
       continue

    #   continue
    if pp == "GAUDI":
      cmd = "svn list " + c.distgaudi + "trunk"
    else:
      cmd = "svn cat " + c.distatlas + "Projects/" + pp + "Release/trunk/cmt/requirements"
    detail(c, cmd)

    subp = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
    stdout,stderr = subp.communicate()
    if subp.returncode != 0:
      info("return code is %s" %subp.returncode)
    if stderr!=None:
      print stderr
      exit(4)

    for a in stdout.splitlines():
      paf = None
      if pp == "GAUDI":
        if a.endswith("/") and a[0] != 'c':  # capture all packages (skip dirs cmt, cmake)
          paf=a.rstrip("/")
##          cmd = "svn list " + c.distgaudi + "tags/" + paf   # get head tag per package
##          detail(c, cmd)
##          subp = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
##          stdout,stderr = subp.communicate()
##          if subp.returncode != 0:
##            info("return code is %s" %subp.returncode)
##          if stderr!=None:
##            print stderr
##            exit(4)
##          sso = stdout.splitlines(); sso.sort()
##          for b in sso:
##            if b.startswith(paf + "-"):
##              ver = b.rstrip("/")
##          lac=[pp, "", paf, ver, c.distgaudi + trunkortag + "/" + paf]
          lac=[pp, "", paf, "", c.distgaudi + trunkortag + "/" + paf]
      else:
        if a.startswith("use "):
          ac=a.split()
          if len(ac) >= 3:
            paf = ac[1]
            if len(ac) >=4:
              lac=[pp, ac[3], paf, ac[2], c.distatlas + ac[3] + "/" + paf + "/" + trunkortag]
            else:
              lac=[pp, "", paf, ac[2], c.distatlas + paf + "/" + trunkortag]
      if paf != None and paf not in pacentered:
        pacentered+=[paf]
        c.cachelist+=[lac]

  c.cachelist.sort()
  
  detail(c, "packages found: %d" %(len(c.cachelist)))
  if c.o_detail2:
    detail(c, "sorted list of packages from release trunk [project, container, package, version, path]:")
    for l in c.cachelist:
      detail(c, l)

  try:                        # save cachelist it to file
    f = open(repofile, 'w')
    pickle.dump(c.cachelist, f)
    detail(c, "Repository contents was written to file %s" %repofile)
  except IOError:
    print "Cannot open file '%s'." % repofile
  return

def doVersionfile(c, head, trunkpth, tagpth, cnt, pck, stdout, tagCMake):
   # Write version.cmake file to package subdirectory after svn checkout
   cmd = 'doVersionfile called with: %s, %s, %s %s' %(trunkpth, tagpth, cnt, pck)
   detail(c, cmd)
   if c.o_tagcmake and tagCMake:
      tagName = tagCMake
   elif head:
      # Look for the revision in the info:
      m = re.search( "revision ([0-9]+)", stdout )
      if m:
         tagName = "%s-r%s" % ( pck, m.group( 1 ) )
      else:
         tagName = "%s-rtrunk" % ( pck )
   else:
      tagName = tagpth.split('/')[-1]
   cmd = 'doVersionfile tag: %s' %(tagName)
   detail(c, cmd)

   if pck.endswith('Release') or pck.endswith('RunTime'):
      pckPathName = pck
   else:
      pckPathName = cnt + pck

   fileName1 = pckPathName+'/version.cmake'
   fileName2 = pckPathName+'/cmt/version.cmt'
   cmd = 'doVersionfile fileName1: %s, fileName2: %s' %(fileName1, fileName2)
   if not detail(c, cmd):
      try:
         f = open(fileName1, 'w')
         f.write(tagName +'\n')
         f.close()

         f = open(fileName2, 'w')
         f.write(tagName +'\n')
         f.close()

         cmd = 'doVersionfile %s, %s written' %(fileName1, fileName2)
         detail(c, cmd)
      except IOError:
         info("Error writing file '%s, %s'." % (fileName1, fileName2))

   return

def get_version(c, package ):
   # Look into cmake release directories and search for packages.txt
   # which stores the package tags of the release

   tag = ''

   # Look into CMAKE_PREFIX_PATH environment
   cpath = os.getenv( "CMAKE_PREFIX_PATH" )
   if not cpath:
      info( "ERROR: CMAKE_PREFIX_PATH empty - no CMake release seems to be set up" )
      return tag

   detail(c, "Looking for package %s in the current release at %s" % (package, cpath))

   found = False
   for cdir in cpath.split( ":" ):
      # Look for packages.txt
      cfilename = os.path.join( cdir, "packages.txt" )
      detail(c, "Release file: %s:\n  " % ( cfilename ) )
      try:
         cfile = open( cfilename, "r" )
      except:
         detail(c, "Could not open file: %s" % ( cfilename ) )
         continue
      # Look for the package:
      #print cfile.read()
      m = re.search( "\n([^\n/]+/)*%s [^\n]+" % package, cfile.read())
      if m:
         detail(c, "Release directory: %s" % ( cdir) )
         tag = m.group( 0 ).split()[1]
         found = True
   
   if not found:
      info( "Package %s not found in packages.txt files at: %s" % ( package, cpath ))
   return tag

def get_version_all(c):
   # Look into cmake release directories and search for packages.txt
   # which stores the package tags of the release
   # return all packages infos for repofile

   tag = ''

   # Look into CMAKE_PREFIX_PATH environment
   cpath = os.getenv( "CMAKE_PREFIX_PATH" )
   if not cpath:
      info( "ERROR: CMAKE_PREFIX_PATH empty - no CMake release seems to be set up" )
      return tag

   detail(c, "Looking for all packages in the current release at %s" % cpath)

   cache = []
   for cdir in cpath.split( ":" ):
      # Look for packages.txt
      cfilename = os.path.join( cdir, "packages.txt" )
      detail(c, "Release file: %s:\n  " % ( cfilename ) )
      try:
         cfile = open( cfilename, "r" )
      except:
         detail(c, "Could not open file: %s" % ( cfilename ) )
         continue
      # Collect package infos
      # ['AtlasCore', 'Event/xAOD', 'xAODEventInfoAthenaPool', 'xAODEventInfoAthenaPool-00-00-04', 'svn+ssh://svn.cern.ch/reps/atlasoff/Event/xAOD/xAODEventInfoAthenaPool/_trunk_tags']
    
      project = cfilename.split("/")[-2]
      for line in cfile.readlines():
         packageInfo = []
         if line.startswith("#"):
            project=line.split()[4]
         else:
            temp = line.split()
            if len(temp) < 1: continue
            path, package = os.path.split(temp[0])

            tag = temp[1]
            paf = c.distatlas + temp[0] + "/" + trunkortag
            packageInfo = [project, path, package, tag , paf ]

         if packageInfo:
            cache+=[packageInfo]

   return cache


def show_version(c, package, tag ):

   info( "Version of %s in the current release: %s" % ( package, tag ))

   return 

def findPkg(c, pkg):
  """Find package in cachelist, return project, container, package, path, tag, headtag."""
  """Work without cachelist whenever possible."""

  coco = pkg.rsplit("/",1)      # split into container/package
  if not c.o_head:
    bpkg = c.o_version          # use version from option
  else:
    bpkg = coco[-1]             # package without container
  npkg = bpkg.split('-',1)[0]   # package without container and version
  if npkg.startswith("Gaudi") or npkg in alsogaudi: # construct path for Gaudi, no container needed
    cpkg = ""
    xprj = "GAUDI"
    xpth = c.distgaudi + trunkortag + "/" + npkg
    if c.o_nocache:
      return xprj, cpkg, npkg, xpth, bpkg, ""
  elif len(coco) > 1:           # athena, container is given, can construct path without using cachelist
    cpkg = cpsl = coco[0]       # container
    if len(cpsl) != 0:
      cpsl = cpkg + "/"
    xprj = ""                   # project unknown
    xpth = c.distatlas + cpsl + npkg + "/" + trunkortag
    if c.o_nocache:
      return xprj, cpkg, npkg, xpth, bpkg, ""
  else:
    cpkg = ""                   # no (not even blank) container, continue with usage of cachelist

  if len(c.cachelist) == 0:     # still need to make cachelist
    do_cache(c)

  fpth=None
  for cp in c.cachelist:
    if cp[2] == npkg:
      fprj=cp[0]
      fcnt=cp[1]
      fpck=cp[2]
      ftag=cp[3]
      fpth=cp[4]
      break
  
  if fpth != None:
    if cpkg != "":         # i.e. a container has been given
      if fcnt != cpkg:     # ...but it seems bad (error out although given container would be ignored)
        raise RuntimeError, "Package '%s' has trunk container '%s'. Bad container given - exit." %(pkg, fcnt)
    if not c.o_head and bpkg == npkg: # i.e. no tag given but tag needed
      bpkg = ftag          # ...use tag from cachelist
    return fprj, fcnt, fpck, fpth, bpkg, ftag
  else:
    raise RuntimeError, "Package '%s' does not exist - exit." % bpkg

      
def checkout(pkg, c):
   """Checkout one package."""

   if c.o_docheckout and c.o_pathlist:       # expect pairs of (path, package) for this option, separated by comma
     pthpkg = pkg.split(",")                 # no further initial checking in this case
     if len(pthpkg) != 2:
       raise RuntimeError, "Expect pair of path,package but got: %s - exit." %pkg
     cmd = "svn co %s %s" %(pthpkg[0],pthpkg[1])
     if not detail(c, cmd):
        p = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
        stdout,stderr = p.communicate()
        if p.returncode != 0:
           info("return code is %s" %p.returncode)
        if stderr!=None:
           print stderr
           exit(4)
     return

   prj,cnt,pck,pth,tag,htag=findPkg(c, pkg)  # now the normal case
   tagCMake = get_version(c, pck)

   if cnt != "":
     cnt += "/"

   if c.o_head:
     head = (tag == pck)         # see if really head version is wanted
   else:
     head = False

# the same for athena and gaudi packages - details are already in pth
   trunkpth = pth.replace(trunkortag, "trunk")
   tagspth = pth.replace(trunkortag, "tags")
   if c.o_tagcmake and tagCMake:
      tagpth = tagspth + "/" + tagCMake
      head = False
   else:
      tagpth = tagspth + "/" + tag
   tagName = ''
   # Remove cnt in case of *Release or *RunTime packages
   if pck.endswith('Release') or pck.endswith('RunTime'):
      pckName = pck
   else:
      pckName = cnt + pck

   if head:
      cmd = "svn co %s %s" %(trunkpth, pckName)
      tagName = trunkpth.split('/')[-1]
   else:
      cmd = "svn co %s %s" %(tagpth, pckName)
      tagName = tagpth.split('/')[-1]

   if c.o_docheckout:
      if not detail(c, cmd):

         info('Working on %s (%s)' %(cnt+pck, tagName))
         info('%s' %(cmd))
         p = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
         stdout,stderr = p.communicate()
         info( stdout )
         if p.returncode != 0:
            info("return code is %s" %p.returncode)
         if stderr!=None:
            print stderr
            exit(4)
         # Write out version file with package tag
         doVersionfile(c, head, trunkpth, tagpth, cnt, pck, stdout, tagCMake)
         info('%s done.' %(cnt+pck))

   else:
      cmd = "svn list %s" %tagspth  # get available versions
      detail(c, cmd)
      info("Project, container, path for %s: %s %s %s" %(pck,prj,cnt,pth))
      info("Tag: %s. Highest available tags:" %tag)
      p = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
      stdout,stderr = p.communicate()
      if p.returncode != 0:
         info("return code is %s" %p.returncode)
      if stderr!=None:
         print stderr
         exit(4)
      vsns = []
      list = stdout.splitlines(); list.sort(None, None, True)  # make sure it is ordered
      nl = 0
      for a in list:
         if a.startswith(pck + "-"):
           b = a.rstrip("/")
           nl += 1
           if nl <= 5:
             info(b)
           vsns += [b]
      if nl > 5:
        info("... etc ...")
      headversion = vsns[0]  # highest
      info("Head version from project trunk: %s, from svn tags: %s" %(htag,headversion))

      if c.o_showrecent and not head and pck != tag:  # makes no sense to compare head with head
         cmd = "svn diff %s %s" %(tagpth,trunkpth)
         detail(c, cmd)
         p = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True, env=c.env)
         stdout,stderr = p.communicate()
         istrunk = False
         if p.returncode != 0:
           info("return code is %s" %p.returncode)
         if stderr!=None:
           print stderr
         else:
           if len(stdout) == 0:
             if p.returncode == 0:
               istrunk = True
           else:
             nl=0
             for l in stdout.splitlines():
               print l
               nl += 1
               if nl > 5:
                 break
             if nl > 5:
               print "... etc ..."
         info("Version %s %s trunk" % (tag, "==" if istrunk else "!="))

      show_version(c, pck, tagCMake)   

   return

   
def safe_checkout(args):
   try:
      checkout(*args)
   except RuntimeError, e:
      print e
      
def main():

   ch = preper()

   try:
      opts,args = getopt.gnu_getopt(sys.argv[1:], "r:csf:dxnutvh", ["help","version"])
   except getopt.GetoptError, e:
      print e
      usage()
      return 1

   # Parse command line
   for o,a in opts:
      if o == "-r":
         ch.o_head = False
         ch.o_version = a
      elif o == "-c":
         ch.o_docheckout = True
      elif o == "-s":
         ch.o_showrecent = True
         ch.o_docheckout = False
         ch.o_nocache = False
      elif o == "-f":
         ch.o_pkgfile = a
      elif o == "-d":
         ch.o_detail = True
      elif o == "-x":
         ch.o_detail = True
         ch.o_detail2 = True
         ch.o_nocache = False
      elif o == "-n":
         ch.o_pathlist = True
      elif o == "-t":
         ch.o_tagcmake = True
      elif o == "-u":
         ch.o_repofl = True
         ch.o_nocache = False
      elif o in ("-v", "--version"):
         print __version__.strip("$")
         return 0
      elif o in ("-h", "--help"):
         usage()
         return 0
      
   if (ch.o_pkgfile is None) and (len(args)==0):
      usage()
      return 1

   # Read optional file with package tags
   pkgList = args
   if ch.o_pkgfile:
      try:
         f = open(ch.o_pkgfile)
      except IOError:
         print "Cannot open file '%s'." % ch.o_pkgfile
         return 2
      for line in f:
         pkgList.append(line.strip())
      f.close()
   if not ch.o_head and len(pkgList) > 1:
      info("for -r, truncate to only one package")
      pkgList = [pkgList[0]]

   if not ch.o_nocache:   # skip this if no repo used
      do_cache(ch)

   # Checkout packages
   args = zip(pkgList,
              [ch]*len(pkgList))

   # allow to process multiple packages in parallel
   if mp and len(pkgList)>1 and ch.o_docheckout and not ch.o_pathlist:
      print "enabling parallel checkout..."
      pool = mp.Pool()
      res = pool.map_async(safe_checkout, args)
      res.get()
   else:
      map(safe_checkout, args)

   return 0

if __name__ == "__main__":
   try:   
      sys.exit(main())
   except RuntimeError, e:
      print e
      sys.exit(1)
   except KeyboardInterrupt:
      sys.exit(1)
