#!/usr/bin/env python

import string
import os
import fileinput
import commands
import sys

verfile = sys.argv[1]

PACKS = [
]

def Default(p, **kw): PACKS.append(p)


Default ('Database/CoraCool')
Default ('Control/AthenaCommon')
Default ('AtlasTest/TestTools')
Default ('Tools/Scripts')
Default ('Control/CxxUtils') #CHANGED [hashmap]
Default ('Control/DataModelRoot')
Default ('Control/AthenaKernel')
Default ('Control/SGMon/SGAudCore')
Default ('Control/SGTools')
Default ('Control/CLIDSvc')
Default ('Control/CLIDComps')
Default ('Control/AthenaExamples/ToyConversion')
Default ('Control/RootUtils')
Default ('Control/AthAllocators')
Default ('Control/AthLinks')
Default ('Control/AthContainersInterfaces')
Default ('Control/AthContainers')
Default ('Control/AthContainersRoot')
Default ('Control/DataModel')
Default ('Control/StoreGate')
Default ('Control/AthenaBaseComps')
Default ('Control/SGComps')
Default ('Control/StoreGateBindings')
Default ('Database/PersistentDataModel')
Default ('Database/APR/POOLCore')
Default ('Database/APR/StorageSvc')
Default ('Database/APR/FileCatalog')
Default ('Database/APR/PersistencySvc')
Default ('Database/APR/XMLCatalog')
Default ('Database/APR/CollectionBase')
Default ('Database/APR/RootCollection')
Default ('Database/APR/TrigCollQuery')
Default ('Database/APR/CollectionUtilities')
Default ('Database/AthenaPOOL/DBDataModel')
Default ('Database/AthenaPOOL/AthenaPoolUtilities')
Default ('Database/IOVDbDataModel')
Default ('Event/xAOD/xAODCore') #CHANGED [dup dict]
Default ('Event/xAOD/xAODEventInfo')
Default ('Event/xAOD/xAODEventFormat')
Default ('Control/xAODRootAccessInterfaces')
Default ('Control/AthToolSupport/AsgTools')
Default ('Tools/PathResolver')
Default ('Database/ConnectionManagement/DBReplicaSvc')
Default ('Database/ConnectionManagement/AtlasAuthentication')
Default ('Database/AthenaPOOL/PoolSvc')
Default ('Control/AthenaPython')
Default ('Event/EventInfo')
Default ('Database/AthenaRoot/AthenaRootKernel')
Default ('Database/AthenaRoot/AthenaRootComps')
Default ('Database/AthenaRoot/RootAuxDynIO')
Default ('Database/APR/RootStorageSvc')
Default ('Database/APR/ImplicitCollection')
Default ('Database/AtlasSTLAddReflex')
Default ('Database/TPTools')
Default ('Database/AthenaPOOL/AthenaPoolCnvSvc') #CHANGED [cmake]
Default ('Database/PersistentDataModelTPCnv')
Default ('Database/PersistentDataModelAthenaPool')
Default ('Database/AthenaPOOL/AthenaPoolServices')
Default ('Database/AthenaPOOL/AthenaPoolKernel')
Default ('Control/PerformanceMonitoring/PerfMonKernel')
Default ('Control/PerformanceMonitoring/PerfMonEvent') #WARN*: malloc hooks
Default ('Control/IOVSvc')
Default ('Control/Navigation')
Default ('Simulation/Tools/AtlasCLHEP_RandomGenerators')
Default ('PhysicsAnalysis/AnalysisCommon/PATCore')
Default ('Control/GaudiSequencer')
Default ('Tools/PyCmt')
Default ('Tools/PyUtils') # CHANGED
Default ('Control/RngComps')
Default ('Control/AthenaServices')
Default ('Database/AthenaPOOL/RootConversions')
Default ('Control/DataModelAthenaPool')
Default ('DetectorDescription/Identifier')
Default ('DetectorDescription/IdDict')
Default ('DetectorDescription/AtlasDetDescr')
Default ('DetectorDescription/GeoModel/GeoModelInterfaces')
Default ('DetectorDescription/GeoModel/GeoModelKernel')
Default ('Database/AthenaPOOL/RDBAccessSvc')
Default ('Database/IOVDbMetaDataTools')
Default ('Event/EventInfoMgt')
Default ('DetectorDescription/GeoModel/GeoModelUtilities')
Default ('DetectorDescription/GeoModel/GeoModelSvc') #CHANGED
Default ('Tools/XMLCoreParser')
Default ('DetectorDescription/IdDictParser')
Default ('InnerDetector/InDetDetDescr/InDetIdDictFiles')
Default ('Control/PileUpTools')
Default ('DetectorDescription/IdDictDetDescr')
Default ('Event/ByteStreamData')
Default ('Event/EventTPCnv')
Default ('Database/AtlasSealCLHEP')
Default ('Database/IOVDbSvc')
Default ('Database/AthenaPOOL/EventSelectorAthenaPool')
Default ('Database/AthenaPOOL/OutputStreamAthenaPool')
Default ('Generators/McEventSelector')
Default ('DetectorDescription/GeometryDBSvc')
Default ('Event/EventPrimitives')
Default ('DetectorDescription/GeoPrimitives') #WARN* ??? ; CHANGED
Default ('InnerDetector/InDetDetDescr/InDetGeoModelUtils')
Default ('InnerDetector/InDetDetDescr/InDetIdentifier')
Default ('InnerDetector/InDetConditions/InDetCondServices')
Default ('MuonSpectrometer/MuonIdHelpers')
Default ('DetectorDescription/DetDescrCond/DetDescrConditions')
Default ('DetectorDescription/DetDescrCond/DetectorStatus')
Default ('Tracking/TrkEvent/TrkEventPrimitives')
Default ('Tracking/TrkDetDescr/TrkDetDescrUtils')
Default ('Tracking/TrkDetDescr/TrkDetElementBase')
Default ('Tracking/TrkEvent/TrkParametersBase')
Default ('Tracking/TrkDetDescr/TrkSurfaces')
Default ('Tracking/TrkEvent/TrkParameters')
Default ('Tracking/TrkEvent/TrkMaterialOnTrack')
Default ('Tracking/TrkEvent/TrkNeutralParameters')
Default ('InnerDetector/InDetConditions/TRT_ConditionsData')
Default ('InnerDetector/InDetDetDescr/InDetReadoutGeometry')
Default ('InnerDetector/InDetDetDescr/PixelGeoModel')
Default ('InnerDetector/InDetDetDescr/BCM_GeoModel')
Default ('InnerDetector/InDetDetDescr/BLM_GeoModel')
Default ('InnerDetector/InDetDetDescr/SCT_GeoModel')
Default ('InnerDetector/InDetConditions/InDetConditionsSummaryService')
Default ('InnerDetector/InDetConditions/InDetByteStreamErrors')
Default ('Database/RegistrationServices')
Default ('InnerDetector/InDetConditions/InDetCoolCoralClientUtils')
Default ('InnerDetector/InDetConditions/TRT_ConditionsServices') #CHANGED [gcc7?]



CLEAN   = 'clean  '
CHANGED = 'changed'


def get_changed ():
    s = set()
    for l in open('SConstruct').readlines():
        while l and l[0] == '#':
            l = l[1:]
        if l.startswith ('Default') and l.find ('CHANGED') >= 0:
            beg = l.find ("'")
            end = l.find ("'", beg+1)
            if beg>=0 and end > beg:
                s.add (l[beg+1:end])
    return s

# (tag, status)
def get_cur_tag (p):
    for l in commands.getoutput ('svn info ' + p).split ('\n'):
        if l.startswith ('URL: '):
            i = l.find ('/tags/')
            if i >= 0:
                return (l[i+6:], None)
            else:
                return (None, 'Not at tag')
    return (None, 'Error getting cur_tag')


def get_new_tags():
    tags = {}
    for l in open(verfile).readlines():
        l = l.strip()
        ipos = l.find(':')
        if ipos < 0: continue
        ll = l[ipos+1:].split()
        if len(ll) != 2: continue
        if ll[1] == '<unknown>': continue
        tags[ll[0]] = ll[1]
    return tags
def get_new_tag (p):
    #base = os.path.basename (p)
    #ver = commands.getoutput ('grep "^%s-" %s|tail -1' % (base, verfile))
    ver = new_tags.get (p, None)
    if not ver:
        return (None, 'Package not in release')
    return (ver, None)


def get_status (p):
    stat = commands.getoutput ('svn status ' + p)
    if not stat:
        return CLEAN
    res = CLEAN
    for l in stat.split ('\n'):
        if l[0] not in 'MAD?' or l[1:8] != '       ':
            return stat
        if l[0] != '?': res = CHANGED
    return res


def doupdate (p):
    (cur_tag, status) = get_cur_tag (p)
    if not cur_tag: return status
    (new_tag, status) = get_new_tag (p)
    if not new_tag: return status

    if cur_tag != new_tag:
        print "***", cur_tag, '->', new_tag
        #os.system ("cd %s; svntocern" % p)
        stat = commands.getoutput ("cd %s; avn switch --accept=postpone @%s" % (p, new_tag))
        print stat
        for l in stat.split('\n'):
            if l.startswith ('svn:'):
                return stat

    return get_status (p)


changed_set = get_changed()
new_tags = get_new_tags()

res = []
for p in PACKS:
    stat = doupdate (p)
    res.append ((p, stat))

print '======================='
for p,stat in res:
    if stat in [CHANGED, CLEAN]:
        if stat == CHANGED and p not in changed_set:
            print stat, 'NOT MARKED', p
        elif stat == CLEAN and p in changed_set:
            print stat, 'MARKED', p
        else:
            print stat, p
    else:
        print '***', p
        print stat


#####

# for p in PACKS:
#     print "***", p
#     base = os.path.basename (p)
#     ver = commands.getoutput ('grep "^%s-" %s|tail -1' % (base, verfile))
#     if not os.path.exists (p):
#         print "    doesn't exist ... skipped!"
#         continue
#     #os.system ("cd %s; svntolocal" % p)
#     os.system ("cd %s; svntocern" % p)
#     os.system ("cd %s; avn switch --accept=postpone @%s" % (p, ver))


# for p in PACKS:
#     print "***", p
#     if not os.path.exists (p):
#         print "    doesn't exist ... skipped!"
#         continue
#     os.system ("cd %s; svn status" % p)
    

