if [ "$BUILDCONFIG" = "" ]; then
  export BUILDCONFIG=$CMTCONFIG
fi
if [ "$TestArea" = "" ]; then
  export TestArea=$PWD
fi
export BUILD=build-$BUILDCONFIG
export BUILDDIR=$TestArea/$BUILD
alias cm-setup=". $HOME/scripts/cm-setup.sh"
cm-setup

if [ "$CXX" = "" ]; then
  cxx=`which g++`
  if [ "$cxx" != "" ]; then
    export CXX=$cxx
  fi
fi

if [ "$CC" = "" ]; then
  cc=`which gcc`
  if [ "$cc" != "" ]; then
    export CC=$cc
  fi
fi

if [ "$FC" = "" ]; then
  fc=`which gfortran`
  if [ "$fc" != "" ]; then
    export FC=$fc
  fi
fi
