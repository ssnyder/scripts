#!/usr/bin/env python

# Make a hex dump of the data in given branches.

import sys
import string

branches = []
evts = []

fname = sys.argv[1]

for a in sys.argv[2:]:
    if a[0] in string.digits:
        evts.append (int(a))
    else:
        branches.append(a)

if len(evts) == 0:
    evts = [0]

    
import ROOT
import PyCintex

f = ROOT.TFile.Open (fname)
t = f.CollectionTree

def getaddr (obj):
    saddr = str(obj).split()[-1]
    if saddr[-1] == '>':
        saddr = saddr[:-1]
    assert (saddr.startswith('0x'))
    return int (saddr[2:],16)


def getbuf (t, bname, entry):
    b = t.GetBranch(bname)
    baddr = getaddr(b)
    b.GetEntry(entry)
    bnum = b.GetReadBasket()
    basket=b.GetBasket(bnum)

    first = ROOT.gInterpreter.Calc('((TBranch*)%s)->GetBasketEntry()[%d]' % (baddr, bnum))

    buf = basket.GetBufferRef()
    end = buf.Length()
    beg = basket.GetEntryPointer (entry - first)

    return (buf, beg, end)


def getfrombuf (buf, offs):
    baddr = getaddr (buf)
    c = ROOT.gInterpreter.Calc ('((TBuffer*)%s)->Buffer()[%d]'%(baddr,offs))
    if c < 0: c = c + 256
    return c


def dumpline (buf, offs, beg, end, f=sys.stdout):
    print >> f, "0x%08x: " % offs,
    for i in range(16):
        if beg+offs >= end: break
        c = getfrombuf (buf, beg+offs)
        offs += 1
        print >> f, "%02x" % c,
    print >> f
    return offs

def dumpbuf (buf, beg, end, f=sys.stdout):
    offs = 0
    while beg+offs < end:
        offs = dumpline (buf, offs, beg, end, f)
    return

def dumpbranch (t, bname, entries, f=sys.stdout):
    print >> f, 'Branch', bname
    for entry in entries:
        print >> f, 'Entry', entry
        (buf, beg, end) = getbuf (t, bname, entry)
        dumpbuf (buf, beg, end, f)
    print >> f
    return

for b in branches:
    dumpbranch (t, b, evts)

