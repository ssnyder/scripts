#!/usr/bin/env python

#
# Usage: cvsstat [-vlR] [files...]
#
#        Like `cvs status', except that it filters the output
#        down to one line/file.
#

import re
import sys
import os
import string

def doit (f):

    lines = []

    re_dir = re.compile ("^cvs (status|server): Examining ([^\n]*)")
    re_status = re.compile ("^cvs (status|server):")
    re_file = re.compile ("^File: ([^ ]*) ([^\n]*)")
    re_error = re.compile ("^cvs [[]")

    path = ""

    while 1:
        l = f.readline ()
        if not l:
            break

        lines.append (l)

        m = re_dir.match (l)
        if m != None:
            newdir = m.group (2)
            if newdir == '.':
                path = ""
            else:
                path = newdir + "/"

        elif re_status.match (l) != None:
            sys.stderr.write (l)

        elif re_file.match (l) != None:
            m = re_file.match (l)
            print "File: %s %s" % (path + m.group (1), m.group (2))

        elif re_error.match (l) != None:
            for ll in lines:
                sys.stderr.write (ll)
            break

def main ():
    xargv = sys.argv[:]
    del xargv[0]
    cmd = "cvs status %s 2>&1" % string.join (xargv)
    f = os.popen (cmd)
    doit (f)
    f.close ()

main ()
