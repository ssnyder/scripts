# This should be sourced.

if [ "$1" = "-f" ]; then
  unset WorkDir_SET_UP
fi

workdir=$BUILDDIR/$BUILDCONFIG
mkdir -p $workdir/bin
mkdir -p $workdir/share
mkdir -p $workdir/lib
mkdir -p $workdir/python
mkdir -p $workdir/jobOptions
mkdir -p $workdir/data
mkdir -p $workdir/include
mkdir -p $workdir/XML


. $AtlasArea/InstallArea/$CMTCONFIG/setup.sh
if [ -r $workdir/setup.sh ]; then
  . $workdir/setup.sh
else
  echo $workdir/setup.sh does not exist.
fi
