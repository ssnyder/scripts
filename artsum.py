#!/usr/bin/env python

from __future__ import print_function
import json
import sys
import os
import glob

def resstring (result, exit_code):
    s = ''
    if exit_code != 0:
        return 'BAD'
    for r in result:
        if r['result'] != 0:
          s = s + 'BAD:' + r['name'] + ' '
    if s == '':
        return 'OK'
    return s


def dumpstat (f):
    o = json.load (open (f))
    for k,v in o.items():
        if k == 'release_info':
            continue
        print (k + ':')
        for ktest, vtest in v.items():
            print ('  ' + resstring(vtest['result'], vtest['exit_code']) + ' ' + ktest)
    return


tag = sys.argv[1]
statfiles = glob.glob (os.path.join (tag, '*status.json'))
for f in statfiles:
    dumpstat (f)
